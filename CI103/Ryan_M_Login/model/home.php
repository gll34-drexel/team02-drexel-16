<?php
session_start();

$_SESSION["username"]=$_POST["uname"];
$_SESSION["password"]=$_POST["upass"];

error_reporting(E_ALL);	
ini_set('display_errors','On');

require('database.php');

include("../view/verify.php");

if (verify_tf($_SESSION["username"],$_SESSION["password"]) == true){
    header("Location: ../onepage.html");
    exit;
}
 else{
     ob_start();
     echo '<script language="javascript">';
     echo 'alert("Wrong Email/Password Combination")';
     echo '</script>';
     header("Location: ../login.html");
 }
