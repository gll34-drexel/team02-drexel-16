<?php
session_start();
 if(!isSet($_SESSION["username"])) header("Location: /login/login_register_page.html");
error_reporting(E_ALL); 
ini_set("display_errors", 1); 
ini_set("html_errors",1);
global $db;
if(!$db)
$db = new SQLite3('../prof.db');
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
      echo "<script>console.log(\"Opened database successfully\");</script>";
   }
$db->enableExceptions(true);
$prof_query = "select * from support;";
$res = $db->query($prof_query);
$arr = array();
while($row = $res->fetchArray(SQLITE3_ASSOC)){
	array_push($arr, $row);
	
}
$jsonarray = json_encode($arr);

?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  
  
  
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>

      <link rel="stylesheet" href="css/style.css">
      <title>Support Ticket Form</title>
      
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="main.js"></script>

</head>

<body>
	<div id="mapfloat"><div id="map"></div></div>
  <style>


  #map{
	height: 100%;
	width: 100%;

	
}
#mapfloat{
		position: fixed;
		left: 0;
		top: 0;
		height: 100%;
		width: 50%;
		
}
  
  
  </style>
  <div id="support-form-modal">
  <div class="modal-dialog" role="document">
	  
    <div class="modal-content">
      <div class="modal-body">
        <h1>Get help</h1>
        <a href="../index.php" class = "btn btn-default">Back</a>
        <form action = "addStudent.php" method = "POST">

            <div class="form-group-lg">
              <label for="email-input">Email address<span class="required">*</span></label>
       <input type="email" class="form-control" id="email" name="email" value="<?php if(isSet($_SESSION["username"]))echo $_SESSION["username"];?>">
            </div>          

              </div>
              <div class="form-group-lg">
                <label for="name-input">Name</label>
                <input type="text" class="form-control" id="name" name="name" value = "<?php if(isSet($_SESSION["username"]))echo $_SESSION["first"]." ". $_SESSION["last"];?>">
              </div>
                  <div class="form-group-lg">
              <label for="address-input">Your Address</label>
              <input type="text" class="form-control" id="address" name="address" placeholder="Please enter your address">
            </div>
              <div class="form-group-lg">
                <label for="product-input">Operating System</label>
                <select class="form-control" id="OS" name="OS">
                  <option></option>
                  <option>Windows 7</option>
                  <option>Windows 8/8.1</option>
                  <option>Windows 10</option>
                  <option>MAC OS X</option>
                  <option>Ubuntu</option>
                  <option>Other</option>            
                </select>
              </div>
        
            <div class="form-group-lg">
              <label for="subject-input">Product Name</label>
              <input type="text" class="form-control" id="product" name="product" placeholder="What can we help you with?">
            </div>
            <div class="form-group-lg">
              <label for="inquiry-input">Problem Description<span class="required">*</span></label>
              <textarea type="text" class="form-control" id="description" name="description" rows="5" placeholder="Please include any relevant links to lists/templates/flows/etc"></textarea>
            </div>
            

          <button type="submit" id="btnAdd" "class="btn btn-default">Submit</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


  
</body>
<script>
var map;
var geocoder;
var loca;

function initMap(){	
	geocoder = new google.maps.Geocoder;
	    console.log("mapinit");
	    makeMap();
		if(navigator.geolocation){
		
		navigator.geolocation.getCurrentPosition(loc, err);
		
		}
		
		
}
function loc(pos){
	 console.log("loc");
	 
	loca = {lat:pos.coords.latitude, lng:pos.coords.longitude};
	map.setCenter(loca);
	
    var infowindow = new google.maps.InfoWindow;
	geocodeLatLng(geocoder, map, infowindow, loca);
	}

function err(){
	console.log("err");
}

function makeMap(){
	map = new google.maps.Map(document.getElementById("map"),{
          zoom: 11,
          center: {lat:40.8,lng:-74},
	      fullscreenControl: true
});
var array = <?php echo $jsonarray;?>;
console.log(array);
array.forEach(addMarkers);

}

function addMarkers(element, index, array){
//marker code here
 var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h3 id="firstHeading" class="firstHeading">' + element.fname + ' ' + element.lname + '</h3>'+
            '<div id="bodyContent">'+
            '<p>'+ element.description + '</p>'+
            '<p><a href="mailto:' + element.email + '">' + element.email + '</a></p>'+
            '</div>'+
            '</div>';
//console.log(contentString);
	var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: {
			  lat: element.latitude,
			  lng:element.longitude,
			
			  },
          map: map,
            icon: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
          title: element.fname + ' ' + element.lname
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

	
}


      function geocodeLatLng(geocoder, map, infowindow, ll) {
        
        var latlng = ll;
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[1]) {
              map.setZoom(11);
              var marker = new google.maps.Marker({
				title: "Your Location",
                position: latlng,
                map: map,
                icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
              });
              infowindow.setContent("Your location: " +  results[1].formatted_address);
              document.getElementById("address").value = results[1].formatted_address;
              infowindow.open(map, marker);
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
      }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkMmGbyi5bE933OBlqdU3QDY3MYtKnyz8&callback=initMap">
    </script>  
</html>
