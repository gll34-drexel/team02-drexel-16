$(document).ready(function(){
    $("#btnAdd").click(function(){
        email = $("#email").val();
        name = $("#name").val();
        address = $("#address").val();
       OS = $("#OS").val();
       product = $("#product").val();
       description = $("#description").val();
        
        $.get("addStudent.php?email=" + email + "&name="+name+"&address="+address+"&OS="+OS+"&product="+product+"&description="+description,
                function(data, status){
                    $("#status").text(data + " record(s), " + email + ", has been added to database.");
                    
                });
    });
    $("#btnReload").click(function(){
        $.get("getAllStudents.php", function(data, status){
            $("#records").empty();
            
            $.each(data, function(index, record){
                $("#records").append("<li>" + record['email'] + ": "
                        + record['subject']
                        + ", " + record['message'] + "</li>");
            });
        });
    });
});


