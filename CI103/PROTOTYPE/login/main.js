$(document).ready(function(){
    $("#btnAdd").click(function(){
        email = $("#email").val();
        upass = $("#upass").val();
        first = $("#first").val();
       last = $("#last").val();
        
        $.get("addStudent.php?email=" + email + "&upass="+upass+"&first="+first+"&last="+last,
                function(data, status){
                    $("#status").text(data + " record(s), " + email + ", has been added to database.");
                    
                });
    });
    $("#btnReload").click(function(){
        $.get("getAllStudents.php", function(data, status){
            $("#records").empty();
            
            $.each(data, function(index, record){
                $("#records").append("<li>" + record['email'] + ": "
                        + record['subject']
                        + ", " + record['message'] + "</li>");
            });
        });
    });
});


