
<?php session_start();
error_reporting(E_ALL);	
ini_set('display_errors','On');


 ?>
<html >
<head>
  <meta charset="UTF-8">
  <title>Home Page</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300'>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700'>

      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
  <div class="line"></div>
<div class="wrapper">
  <header role="banner">
    <nav role="navigation">
      <h1><a href="#">TechLynk</a></h1>
      <ul class="nav-ul">
        <li><a href="#">Home</a></li>
        <li><a href="about/index.html">About Us</a></li>
        <li><?php if(isSet($_SESSION["username"])and($_SESSION["username"]=="admin@gmail.com")){echo '<a href="admin.php">Admin';}else{echo '<a href="support/index.php">Support';}?></a></li>
        <li><a href="contact/index.html">Contact Us</a></li>
        <li><a href="<?php  if(!isSet($_SESSION["username"])) {echo 'login/login_register_page.html">Login';}else{echo 'logout.php">Logout Welcome '. $_SESSION["first"]." ". $_SESSION["last"];}?></a></li>
      </ul>
    </nav>
  </header>
  <main role="main">
    <section class="sec-intro" role="section">
      <img src="http://www.onlyhdpic.com/images/Collections/hd-pics-photos-technology-matrix-world-map-green-desktop-background-wallpaper.jpg" alt="" />
      <h1>Welcome to the future!</h1>
    </section>
    <section class="sec-boxes" role="section">
      <adrticle class="box">
        <h1>The Future</h1>
        <p>The future is here, and we welcome you to it. We are a leading innovator of wha we believe to call the "Future Product". It is on a need to know basis.</p>
        <button class="button" type="button" role="button" value="MORE" onclick="location.href='about/index.html';">More</button>
      </adrticle>
      <adrticle class="box">
        <h1>Dedicated</h1>
        <p>We are a new company that is rapidy growing. We our dedicated to bringing you the most efficient and swift user experience on the market, and solve any related computer problems you may have.</p>
        <button class="button" type="button" role="button" value="MORE" onclick="location.href='about/index.html';">More</button>
      </adrticle>
      <adrticle class="box">
        <h1>User Friendly</h1>
        <p>.If you have any concerns you can always contact us. We will respond as fast as possible to help fit your needs, whether it be dissatisfaction or a late delivery.</p>
        <button class="button" type="button" role="button" value="MORE" onclick="location.href='contact/index.html';">More</button>
      </adrticle>
      <adrticle class="box">
        <h1>Experienced</h1>
        <p>Our expeirenced employees can help you with any problems you may have in a quick and efficient manner. Welcome to the future.</p>
        <button class="button" type="button" role="button" value="MORE" onclick="location.href='support/index.php';">More</button>
      </adrticle>
    </section>
  </main>
</div>
<footer>
  <nav role="navigation">
    <ul class="nav-ul">
      <li><a href="#">Home</a></li>
      <li><a href="about/index.html">About Us</a></li>
      <li><a href="support/index.php">Support</a></li>
      <li><a href="contact/index.html">Contact Us</a></li>
      <li><a href="login/login_register_page.html">Login</a></li>
    </ul>
  </nav>
  <p class="copy">&copy; TechLynk</p>
</footer>
<div class="line"></div>
  
  
</body>
</html>
